<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class IncaperServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('incaper', function ($app) {
            $config = $app->make('config')->get('apis.incaper');

            return new Client(['base_uri' => Arr::get($config, 'url')]);
        });
    }
}
