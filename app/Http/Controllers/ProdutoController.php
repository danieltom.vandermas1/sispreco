<?php

namespace App\Http\Controllers;

use App\Http\Resources\Produto as ProdutoResource;
use App\Models\Produto;

class ProdutoController extends Controller
{
    public function all()
    {
        return ProdutoResource::collection(Produto::all());
    }

    public function show($id)
    {
        return ProdutoResource::make(Produto::find($id)->first());
    }

    public function get()
    {
        return [
            'data' => Produto::getAll(),
        ];
    }
}
