<?php

namespace App\Http\Controllers;

use App\Http\Resources\Beneficiario as BeneficiarioResource;
use App\Models\Beneficiario;
use App\Models\Municipio;

class BeneficiarioController extends Controller
{
    public function all()
    {
        return BeneficiarioResource::collection(Beneficiario::all());
    }

    public function municipio($id)
    {
        $municipio = Municipio::where('cd_codigo_ibge', $id)->get(['id_municipio'])->first();

        return BeneficiarioResource::collection(Beneficiario::where('id_municipio_FK', 2)->get(['id_beneficiario', 'nm_nome', 'nm_apelido', 'nm_telefone']));
    }

    public function show($id)
    {
        return [
            'data' => BeneficiarioResource::make(Beneficiario::find($id)->first()),
        ];
    }

    public function get($id)
    {
        return [
            'data' => Beneficiario::getFromIncaper($id),
        ];
    }
}
