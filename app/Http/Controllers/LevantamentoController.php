<?php

namespace App\Http\Controllers;

use App\Models\IntervaloPreco;
use App\Models\Levantamento;
use App\Models\Producao;
use App\Models\Produto;
use App\Models\ProdutoLevantamento;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LevantamentoController extends Controller
{
    public function find(Request $request)
    {
        $request->validate([
            'produto' => 'required',
            'produtor' => 'required',
            'municipio' => 'required',
            'ano' => 'required',
            'mes' => 'required',
            'semana' => 'required',
        ]);

        $levantamentos = Levantamento::select(['autor', 'preco'])->where($request->all())->get()->toArray();

        $levantamentos = array_map(function ($levantamento) {
            return [
                'Servidor' => $levantamento['autor'],
                'Preço Cadastrado' => 'R$ ' . number_format($levantamento['preco'] / 100.0, 2, ',', ''),
            ];
        }, $levantamentos);

        return [
            'data' => $levantamentos,
        ];
    }

    public function create(Request $request)
    {
        $request->validate([
            'produto' => 'required',
            'produtor' => 'required',
            'municipio' => 'required',
            'preco' => 'required',
            'autor' => 'required',
        ]);

        $now = Carbon::now();

        $data = array_merge($request->only([
            'produto',
            'produtor',
            'municipio',
        ]), [
            'semana' => $now->weekOfMonth,
            'mes' => $now->month,
            'ano' => $now->year,
        ]);

        $preco = $request->input('preco');
        $autor = $request->input('autor');

        $levantamento = Levantamento::where($data)->first();

        if ($levantamento) {
            $levantamento->autor = $autor;
            $levantamento->preco = $preco;
            $levantamento->save();
        } else {
            $data['autor'] = $autor;
            $data['preco'] = $preco;

            $levantamento = Levantamento::create($data);
        }

        return response()->json([
            'error' => false,
            'data' => $levantamento,
        ], 201);
    }

    public function getAnosLevantamento()
    {
        return [
            'data' => array_pluck(Levantamento::select(['ano'])->distinct('ano')->get()->toArray(), 'ano'),
        ];
    }

    /**
     * Retorna a lista de produtos a serem feitos o levantamento de preço em um município.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getProdutos(Request $request)
    {
        $request->validate([
            'municipio' => 'required',
        ]);

        $municipio = $request->get('municipio');

        $produtos = $this->produtosLevantamento($municipio);

        return response()->json([
            'error' => false,
            'data' => $produtos,
        ]);
    }

    /**
     * Atualiza a lista de produtos a serem feitos o levantamento de preço em um município.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postProdutos(Request $request)
    {
        $request->validate([
            'municipio' => 'required',
            'produtos' => 'required',
        ]);

        $municipio = $request->get('municipio');
        $produtos = $request->get('produtos');
        $ano = Carbon::today()->year;

        ProdutoLevantamento::where([
            'municipio' => $municipio,
            'ano' => $ano,
        ])->delete();

        foreach ($produtos as $produto) {
            ProdutoLevantamento::create([
                'municipio' => $municipio,
                'ano' => $ano,
                'produto' => $produto,
            ]);
        }

        return response()->json([
            'error' => false,
            'data' => $this->produtosLevantamento($municipio),
        ]);
    }

    /**
     *
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getIntervaloPreco(Request $request)
    {
        return response()->json([
            'error' => false,
            'intervalos' => IntervaloPreco::all(['produto', 'minimo', 'maximo']),
        ]);
    }

    /**
     *
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postIntervaloPreco(Request $request)
    {
        $request->validate([
            'produto' => 'required',
            'minimo' => 'required',
            'maximo' => 'required',
        ]);

        $produto = $request->get('produto');
        $minimo = $request->get('minimo');
        $maximo = $request->get('maximo');

        $instancia = IntervaloPreco::where(['produto' => $produto])->first();

        if ($instancia) {
            $instancia->minimo = $minimo;
            $instancia->maximo = $maximo;
            $instancia->save();
        } else {
            IntervaloPreco::create([
                'produto' => $produto,
                'minimo' => $minimo,
                'maximo' => $maximo,
            ]);
        }

        return response()->json([
            'error' => false,
        ]);
    }

    /**
     * Determina quais produtos devem ser selecionados no município baseado na produção.
     *
     * @param int $municipio
     * @param int $ano
     *
     * @return array
     */
    private function determinaProdutos($municipio, $ano)
    {
        $produtos = array_pluck(Producao::select('produto')
            ->where([
                'ano' => $ano,
                'municipio' => $municipio,
            ])
            ->groupBy('produto')
            ->get(), 'produto');

        $produtosSelecionados = [];

        foreach ($produtos as $produto) {
            $producoes = Producao::selectRaw('sum(quantidade) as quantidade, municipio')
                ->where([
                    'ano' => $ano,
                    'produto' => $produto,
                ])
                ->groupBy('municipio')
                ->orderBy('quantidade', 'desc')
                ->get();

            $quantidadeTotal = Producao::where([
                'ano' => $ano,
                'produto' => $produto,
            ])->sum('quantidade');

            $somaQuantidade = 0;

            foreach ($producoes as $producao) {
                $quantidade = intval($producao['quantidade']);
                $municipioProducao = $producao['municipio'];

                $somaQuantidade += $quantidade;

                if ($municipio == $municipioProducao && ($somaQuantidade < $quantidadeTotal * 0.7 || $quantidade >= $quantidadeTotal * 0.7)) {
                    $produtosSelecionados[] = $produto;
                }
            }
        }

        return array_values($produtosSelecionados);
    }

    /**
     * Retorna os produtos selecionados para o levantamento em um município,
     * se nenhum produto foi selecionado ainda, retorna as sugestões baseadas na produção.
     *
     * @param int $municipio
     * @param int $ano
     *
     * @return array
     */
    public function produtosSelecionados($municipio, $ano)
    {
        $produtos = ProdutoLevantamento::where([
            'municipio' => $municipio,
            'ano' => $ano,
        ])->get()->toArray();

        if ($produtos) {
            return array_pluck($produtos, 'produto');
        }

        return $this->determinaProdutos($municipio, $ano - 1);
    }

    public function produtosLevantamento($municipio)
    {
        $produtosSelecionados = $this->produtosSelecionados($municipio, Carbon::today()->year);
        $todosProdutos = Produto::getAll();

        return array_values(array_filter($todosProdutos, function ($produto) use ($produtosSelecionados) {
            return in_array($produto['id'], $produtosSelecionados);
        }));
    }
}
