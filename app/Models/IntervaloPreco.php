<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IntervaloPreco
 *
 * @property string $produto
 * @property int $minimo
 * @property int $maximo
 *
 * @package App\Models
 */
class IntervaloPreco extends Eloquent
{
    protected $connection = 'sispreco';
    protected $table = 'intervalos_preco';
    protected $primaryKey = 'produto';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'produto',
        'minimo',
        'maximo',
    ];
}
