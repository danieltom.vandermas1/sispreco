<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UnidadeFederativa
 *
 * @property int $id_unidade_federativa
 * @property string $nm_unidade_federativa
 * @property string $nm_sigla
 * @property int $cd_codigo_ibge
 *
 * @property \Illuminate\Database\Eloquent\Collection $municipios
 *
 * @package App\Models
 */
class UnidadeFederativa extends Eloquent
{
    protected $connection = 'incaper';
    protected $table = 'unidades_federativas';
    protected $primaryKey = 'id_unidade_federativa';
    public $timestamps = false;

    protected $casts = [
        'cd_codigo_ibge' => 'int'
    ];

    protected $fillable = [
        'nm_unidade_federativa',
        'nm_sigla',
        'cd_codigo_ibge'
    ];

    public function municipios()
    {
        return $this->hasMany(\App\Models\Municipio::class, 'id_unidade_federativa_FK');
    }
}
